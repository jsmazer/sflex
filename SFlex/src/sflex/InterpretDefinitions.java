/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sflex;

import java.util.Scanner;

/**
 *
 * @author JMazer
 */
public class InterpretDefinitions {
    private void handleOptions(String option_line){
        Scanner sc = new Scanner(option_line);
        String option = sc.next();
        
        switch(option){
            case "class":{
                Utility.sflex_name = sc.next();
                Utility.createFile(Utility.sflex_name);
                break;
            }
        }
    }
    
    public void analyzeLine(String line){        
        char c = line.charAt(0);
        char look_ahead = '\0';
        
        if(c == '%'){
            look_ahead = line.charAt(1);
            if(look_ahead == '{'){ //input code, and imports
                //handle imports and input code here
            } else { //handle options
                String option_line = line.substring(1, line.length());
                handleOptions(option_line);
            }
        }
    }
}
