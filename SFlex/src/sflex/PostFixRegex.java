/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sflex;

import java.util.Stack;

/**
 *
 * @author JMazer
 */
public class PostFixRegex {
    private String regex;
    private Stack<String> regexStack;
    private Stack<Op> opStack;
    private final char[] regexOps = {'+', '*', '?', '^', '.', '[', ']', '(', ')', '|'};
    
    public PostFixRegex(String regex){
        this.regex = regex;
    }
    
    public String postFixLine(){
        for(int i=0;i<regex.length();i++){
            char c = regex.charAt(i);
            char look_ahead = '\0';
            
            if(i!=regex.length()-1)
                look_ahead = regex.charAt(i+1);
            
            //put this line back in
            //i = regexCases(c, look_ahead, regex, i);
        }
        
        while(!opStack.isEmpty()){ //pop the remaining operators and put them on the regexStack
            Op temp_op = opStack.pop();
            regexStack.push(temp_op.toString());
        }
        
        return null;
    }
}
