/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sflex;

/**
 *
 * @author JMazer
 */
public class InterpretRules {
    private int index_ptr;
    
    public InterpretRules(){
        index_ptr = 0;
    }
    
    private String getRegex(String line){
        char c = line.charAt(index_ptr);
        
        //do error checking here to see if rule has no spaces
        
        while(!Character.isWhitespace(c)){
            c = line.charAt(index_ptr);
            index_ptr++;
        }
        int end = index_ptr-1;
        
        //set index ptr to the start of action
        while(Character.isWhitespace(c)){
            c = line.charAt(index_ptr);
            index_ptr++;
        }
        index_ptr = index_ptr-1;
        
        String regex = line.substring(0, end);
        return regex;
    }
    
    private String getAction(String line){
        int begin = index_ptr;        
        String action = line.substring(begin, line.length());
        return action;
    }
    
    public void analyzeLine(String line){
        String regex = getRegex(line);
        String action = getAction(line);
        
        PostFixRegex pfr = new PostFixRegex(regex);
        
        
    }
}
