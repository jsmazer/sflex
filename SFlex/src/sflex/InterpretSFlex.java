/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sflex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JMazer
 */
public class InterpretSFlex {
    private String sflex_file;
    
    public InterpretSFlex(String arg0){ //constructor
        int length = arg0.length();
        if(length < 6) {
            System.err.println("Bad file");
            System.exit(1);
        }
        
        String file_type = arg0.substring(length-5, length);
        if(!file_type.equals("sflex")){
            System.err.println("Bad file");
            System.exit(1);
        }
        
        String path = new File("").getAbsolutePath();
        path = path + "/src/sflex/";
        
        sflex_file = path + arg0;
    }
    
    
    public void interpret() {
        FileReader fr = null;
        try {
            fr = new FileReader(sflex_file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InterpretSFlex.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader br = new BufferedReader(fr);
        
        InterpretDefinitions id = new InterpretDefinitions();
        InterpretRules ir = new InterpretRules();
        InterpretUserCode iuc = new InterpretUserCode();
        
        String line = null;
        try {
            line = br.readLine(); 
            while(!line.equals("%%")){ //interpret definitions
                if(!line.isEmpty()){
                    //System.out.println(line);
                    id.analyzeLine(line);
                }
                line = br.readLine();
            }
            
            //create deafult file if class option is not specified
            if(Utility.sflex_file == null){
                Utility.sflex_name = "sflex";
                Utility.createFile(Utility.sflex_name);
            }
            
            //TEst to see if File exists
            if(Utility.sflex_file.exists()){
                System.out.println("Created file \"search.java\"");
            }
            
            
            //initialize file with switch statement
            InitializeFile ifl = new InitializeFile();
            ifl.initialize();

            line = br.readLine(); //read line after first %%
            while(!line.equals("%%")){ //interpret rules
                if(!line.isEmpty()){
                    //System.out.println(line);
                    ir.analyzeLine(line);
                }
                line = br.readLine();
            }
            
            line = br.readLine(); //read line after second %%
            while(line != null && !line.equals("%%")){ //interpret user code
                if(!line.isEmpty()){
                    //System.out.println(line);
                    //iuc.analyzeLine(line);
                }
                line = br.readLine();
            }

            if(fr != null){
                fr.close();
                br.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(InterpretSFlex.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
