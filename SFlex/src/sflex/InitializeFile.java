/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sflex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JMazer
 */
public class InitializeFile {
    private ArrayList<String> file_lines;
    
    public InitializeFile(){
        file_lines = new ArrayList<>();
    }
    
    private void addYYLex(){
        String yylex = "\tpublic static void yylex(){\n";
        file_lines.add(yylex);
        
        //add switch statement in later
        
        String end_yylex = "\t}\n";
        file_lines.add(end_yylex);
    }
    
    private void writeToLex(){
        for(String str: file_lines){
            try {
                Utility.sflex_fw.write(str);
            } catch (IOException ex) {
                Logger.getLogger(InitializeFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void initialize(){
        String class_line = "public class " + Utility.sflex_name + "{" + "\n";
        file_lines.add(class_line);
        
        addYYLex();

        String end_class = "}";
        file_lines.add(end_class);
        
        writeToLex();
    }
}
