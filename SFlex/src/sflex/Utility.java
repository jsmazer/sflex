/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sflex;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JMazer
 */
public class Utility {
    public static File sflex_file = null;
    public static FileWriter sflex_fw = null;
    
    public static String sflex_name = null;
  
    public static void createFile(String file_name){
        String path = new File("").getAbsolutePath();
        String file_path = path + "/" + file_name + ".java";
        sflex_file = new File(file_path);
        
        try {
            sflex_file.createNewFile();
            sflex_fw = new FileWriter(sflex_file);
        } catch (IOException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
